<?php
/**
 * @file
 * Definition of Media23VideoStreamWrapper object.
 */

/**
 * Class Media23VideoStreamWrapper.
 */
class Media23VideoStreamWrapper extends MediaReadOnlyStreamWrapper {

  /**
   * Handles parameters on the URL string.
   *
   * @see MediaReadOnlyStreamWrapper::interpolateUrl()
   */
  public function interpolateUrl() {
    $parameters = $this->get_parameters();

    return empty($parameters) ? '' : "http://{$parameters['s']}/v.ihtml?source=share&photo_id={$parameters['v']}&autoPlay=0";
  }

  /**
   * Returns the MIME type of the resource.
   *
   * @see MediaReadOnlyStreamWrapper::getMimeType()
   */
  public static function getMimeType($uri, $mapping = NULL) {
    return MEDIA_23VIDEO_MIME;
  }

  /**
   * Get the video HTML.
   *
   * @param array $params
   *   Parameters for {@link getOEmbed()} method.
   *
   * @return string
   *   HTML code of an IFRAME element with a video.
   */
  public function getHtml(array $params = array()) {
    return $this->getEmbedInfo('html', $params);
  }

  /**
   * Get the external video thumbnail.
   *
   * @param array $params
   *   Parameters for {@link getOEmbed()} method.
   *
   * @return string
   *   External path to video thumbnail.
   */
  public function getOriginalThumbnailPath(array $params = array()) {
    return $this->getEmbedInfo('thumbnail_url', $params);
  }

  /**
   * Get the local video thumbnail.
   *
   * @return string
   *   Local path to video thumbnail.
   */
  public function getLocalThumbnailPath() {
    $parameters = $this->get_parameters();
    // There's no need to hide thumbnails, always use the public system rather
    // than file_default_scheme().
    $dir = 'public://' . MEDIA_23VIDEO_SERVICE;
    $path = "$dir/" . check_plain($parameters['v']) . '.jpg';

    if (!file_exists($path) && file_prepare_directory($dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
      $path = file_unmanaged_save_data(file_get_contents($this->getOriginalThumbnailPath()), $path);
    }

    return empty($path) ? '' : $path;
  }

  /**
   * Get the value of response parameter.
   *
   * @param string $param
   *   The name of parameter from response.
   * @param array $params
   *   Parameters for {@link getOEmbed()} method.
   *
   * @return string
   *   Parameter value.
   */
  private function getEmbedInfo($param = '', array $params = array()) {
    $parameters = $this->get_parameters();
    $info = array();

    if (!empty($parameters)) {
      $handler = new Media23VideoInternetHandler("http://{$parameters['s']}/video/{$parameters['v']}");
      $info = $handler->getOEmbed($params);
    }

    return isset($info[$param]) ? $info[$param] : '';
  }

}
