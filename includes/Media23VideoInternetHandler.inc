<?php
/**
 * @file
 * Definition of Media23VideoInternetHandler object.
 */

/**
 * Class Media23VideoInternetHandler.
 *
 * @see media_23video_media_internet_providers()
 */
class Media23VideoInternetHandler extends MediaInternetBaseHandler {

  protected static $processedEmbedCodes = array();

  /**
   * Check if a 23Video video ID valid.
   *
   * @param string $embed_code
   *   23Video embed code.
   *
   * @return string
   *   23video stream wrapper URI.
   */
  public function parse($embed_code) {
    if (!empty(self::$processedEmbedCodes[$embed_code][__FUNCTION__])) {
      return self::$processedEmbedCodes[$embed_code][__FUNCTION__];
    }

    $embed_code = rawurldecode($embed_code);

    if (preg_match('/src="([^"]+)/i', $embed_code, $matches)) {
      $url_parts = parse_url($matches[1]);

      if (isset($url_parts['query'])) {
        parse_str($url_parts['query'], $query);

        if (isset($query['photo_id'])) {
          $this->embedCode = $url_parts['scheme'] . '://' . $url_parts['host'] . '/video/' . $query['photo_id'];
        }
      }
    }

    $info = $this->getOEmbed();

    if (!empty($info['provider_url']) && !empty($info['photo_id']) && 'video' == $info['type']) {
      return self::$processedEmbedCodes[$embed_code][__FUNCTION__] = file_stream_wrapper_uri_normalize(
        MEDIA_23VIDEO_SERVICE . '://v/' . $info['photo_id'] . '/s/' . parse_url($info['provider_url'], PHP_URL_HOST)
      );
    }

    return FALSE;
  }

  /**
   * Determines if this handler should claim the item.
   *
   * @see MediaInternetBaseHandler::claim()
   *
   * @param string $embed_code
   *   23Video embed code.
   *
   * @return bool
   *   Claim status.
   */
  public function claim($embed_code) {
    return (bool) $this->parse($embed_code);
  }

  /**
   * Returns a file object which can be used for validation.
   *
   * @see MediaInternetBaseHandler::getFileObject()
   *
   * @return \stdClass
   *   Drupal file object.
   */
  public function getFileObject() {
    $file = file_uri_to_object($this->parse($this->embedCode), TRUE);

    if (empty($file->fid)) {
      $info = $this->getOEmbed();

      if (!empty($info['title'])) {
        $file->filename = truncate_utf8($info['title'], 255);
      }
    }

    return $file;
  }

  /**
   * Returns information about the media.
   *
   * @see url()
   * @link http://www.oembed.com/
   *
   * @param array $params
   *   URL query parameters.
   *
   * @return array
   *   Embed data.
   */
  public function getOEmbed(array $params = array()) {
    $cache_key = implode(':', array_merge(array_values($params), array_keys($params), array($this->embedCode)));
    $cache = cache_get($cache_key);

    if (empty($cache->data)) {
      $url = parse_url($this->embedCode);

      if (isset($url['host'])) {
        $request = url($url['scheme'] . '://' . $url['host'] . '/oembed', array(
          'query' => $params + array(
            'url' => $this->embedCode,
            'format' => 'json',
          ),
        ));

        $response = drupal_http_request($request);

        if (empty($response->error)) {
          $data = drupal_json_decode($response->data);

          cache_set($cache_key, $data);

          return $data;
        }
        else {
          watchdog(MEDIA_23VIDEO_MODULE_TITLE, 'The next error was received when requesting on @url: @error', array(
            '@url' => $request,
            '@error' => $response->error,
          ), WATCHDOG_ERROR);
        }
      }
    }
    else {
      return $cache->data;
    }

    return array();
  }

}
