<?php
/**
 * @file
 * 23Video service for Media.
 */

/**
 * Module human name.
 */
define('MEDIA_23VIDEO_MODULE_TITLE', t('23Video'));
/**
 * Module machine name.
 */
define('MEDIA_23VIDEO_MODULE_NAME', 'media_23video');
/**
 * Service machine name.
 */
define('MEDIA_23VIDEO_SERVICE', '23video');
/**
 * Service MIME type.
 */
define('MEDIA_23VIDEO_MIME', 'video/' . MEDIA_23VIDEO_SERVICE);
/**
 * The name of object, implemented the Stream Wrapper.
 */
define('MEDIA_23VIDEO_STREAM_WRAPPER', 'Media23VideoStreamWrapper');
/**
 * The name of object, implemented the Internet Handler.
 */
define('MEDIA_23VIDEO_PROVIDER', 'Media23VideoInternetHandler');

/**
 * Implements hook_media_internet_providers().
 */
function media_23video_media_internet_providers() {
  return array(
    MEDIA_23VIDEO_PROVIDER => array(
      'title' => MEDIA_23VIDEO_MODULE_TITLE,
    ),
  );
}

/**
 * Implements hook_file_mimetype_mapping_alter().
 */
function media_23video_file_mimetype_mapping_alter(array &$mapping) {
  $mapping['mimetypes'][] = MEDIA_23VIDEO_MIME;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function media_23video_ctools_plugin_api($owner, $api) {
  if ('file_entity' == $owner && 'file_default_displays' == $api) {
    return array('version' => 1);
  }
}

/**
 * Implements hook_stream_wrappers().
 */
function media_23video_stream_wrappers() {
  return array(
    MEDIA_23VIDEO_SERVICE => array(
      'type' => STREAM_WRAPPERS_READ_VISIBLE,
      'name' => MEDIA_23VIDEO_MODULE_TITLE,
      'class' => MEDIA_23VIDEO_STREAM_WRAPPER,
      'description' => t('Videos provided by !service.', array('!service' => MEDIA_23VIDEO_MODULE_TITLE)),
    ),
  );
}

/**
 * Implements hook_media_parse().
 *
 * @todo This hook should be deprecated. Refactor Media module to not call it
 *   any more, since media_internet should be able to automatically route to the
 *   appropriate handler.
 */
function media_23video_media_parse($embed_code) {
  $class = MEDIA_23VIDEO_PROVIDER;
  $handler = new $class($embed_code);

  return $handler->parse($embed_code);
}

/**
 * Implements hook_file_formatter_info().
 */
function media_23video_file_formatter_info() {
  $formatters = array();

  foreach (_media_23video_get_settings('titles') as $formatter => $title) {
    $callbacks = array();

    foreach (array('view', 'settings') as $type) {
      $callbacks["$type callback"] = MEDIA_23VIDEO_MODULE_NAME . '_file_formatter_' . $formatter . '_' . $type;
    }

    $formatters[MEDIA_23VIDEO_MODULE_NAME . '_' . $formatter] = $callbacks + array(
      'label' => $title,
      'file types' => array('video'),
      'mime types' => array(MEDIA_23VIDEO_MIME),
      'default settings' => _media_23video_get_settings($formatter),
    );
  }

  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 *
 * @see media_23video_file_formatter_info()
 */
function media_23video_file_formatter_video_view(\stdClass $file, array $display, $langcode) {
  if (MEDIA_23VIDEO_SERVICE == file_uri_scheme($file->uri)) {
    return array(
      '#markup' => file_stream_wrapper_get_instance_by_uri($file->uri)->getHtml($display['settings']),
    );
  }

  return NULL;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_23video_file_formatter_image_view(\stdClass $file, array $display, $langcode) {
  if (MEDIA_23VIDEO_SERVICE == file_uri_scheme($file->uri)) {
    $element = array();
    $image_style = $display['settings']['image_style'];
    $image_styles = image_style_options(FALSE);

    $element['#path'] = file_stream_wrapper_get_instance_by_uri($file->uri)->getLocalThumbnailPath();
    $element['#alt'] = isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename;

    if (empty($image_styles[$image_style])) {
      $element = array('#theme' => 'image');
    }
    else {
      $element = array(
        '#theme' => 'image_style',
        '#style_name' => $image_style,
      );
    }

    return $element;
  }

  return NULL;
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_23video_file_formatter_video_settings(array $form, array &$form_state, array $settings) {
  return _media_23video_set_settings('video', $settings);
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_23video_file_formatter_image_settings(array $form, array &$form_state, array $settings) {
  return _media_23video_set_settings('image', $settings);
}

/**
 * Grouped settings for available file formatters.
 *
 * @param string $type
 *   Formatter type. Can be the "video", "image" or "titles".
 *
 * @return array
 *   Form elements for file formatters.
 */
function _media_23video_settings_form($type) {
  $elements = array();

  foreach (array(
    'video' => array(
      'maxwidth' => array(
        '#type' => 'textfield',
        '#title' => t('Width'),
        '#default_value' => 480,
      ),
      'maxheight' => array(
        '#type' => 'textfield',
        '#title' => t('Height'),
        '#default_value' => 270,
      ),
      'autoplay' => array(
        '#type' => 'checkbox',
        '#title' => t('Autoplay'),
        '#default_value' => 0,
      ),
    ),
    'image' => array(
      'image_style' => array(
        '#type' => 'select',
        '#title' => t('Image style'),
        '#options' => image_style_options(FALSE),
        '#empty_option' => t('None (original image)'),
        '#default_value' => '',
      ),
    ),
  ) as $formatter => $fields) {
    $elements[$formatter] = $fields;
    $elements['titles'][$formatter] = array(
      '#default_value' => sprintf('%s %s', MEDIA_23VIDEO_MODULE_TITLE, ucfirst($formatter)),
    );
  }

  return empty($elements[$type]) ? $elements : $elements[$type];
}

/**
 * Get default values for formatter form elements.
 *
 * @param string $type
 *   Formatter type. Can be the "video", "image" or "titles".
 *
 * @return array
 *   Default settings for formatter.
 */
function _media_23video_get_settings($type) {
  $settings = array();

  foreach (_media_23video_settings_form($type) as $name => $field) {
    $settings[$name] = $field['#default_value'];
  }

  return $settings;
}

/**
 * Update default values on form elements.
 *
 * @param string $type
 *   Formatter type. Can be the "video" or "image".
 * @param array $settings
 *   An array with values for formatter settings.
 *
 * @return array
 *   Updated form elements.
 */
function _media_23video_set_settings($type, array $settings) {
  $elements = array();

  foreach (_media_23video_settings_form($type) as $name => $element) {
    if (isset($settings[$name])) {
      $element['#default_value'] = $settings[$name];
    }

    $elements[$name] = $element;
  }

  return $elements;
}
